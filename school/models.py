from django.db import models

# Create your models here.


class School(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    school_name = models.CharField(max_length=100)
    block_name = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    moi = models.CharField(max_length=100, default="Others")
    district_name = models.CharField(max_length=100)

    def __str__(self):
        return self.school_name+" : "+self.category
