from django.core.management.base import BaseCommand, CommandError
from school.models import School as school
from school.form import SchoolForm
import csv


class Command(BaseCommand):

    help = 'Command use for insert csv data to mysql'

    def add_arguments(self, parser):
        parser.add_argument('csv', nargs='+', type=str)

    def handle(self, *args, **options):
        print(options['csv'][0])
        schools = csv.DictReader(open(options['csv'][0]))
        for school_detail in schools:
            school_dict = dict(school_detail)
            school_data = {}
            school_data['id'] = school_dict['id']
            school_data['category'] = school_dict['cat']
            school_data['moi'] = school_dict['moi']
            if school_dict['moi'] == "":
                school_data['moi'] = "Others"
            school_data['district_name'] = school_dict['district_name']
            school_data['school_name'] = school_dict['name']
            school_data['block_name'] = school_dict['block_name']
            SchoolForm(school_data).save()
            # print(school_data)

        self.stdout.write("Csv to mysql data is inserted")
