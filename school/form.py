from django import forms
from .models import School


class SchoolForm(forms.ModelForm):
    class Meta:
        model = School
        fields = ['id', 'school_name', 'district_name',
                  'block_name', 'category', 'moi']
