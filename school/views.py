from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.template import loader


def Hp1View(request):
    return HttpResponse(loader.get_template('hp1.html').render({}, request))


def Hp1JsonView(request):
    print('Hp1JsonView')
    return HttpResponse(loader.get_template('hp1.html').render({}, request))
