from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^hp1/', views.Hp1View, name='hp1'),
    url(r'^gethp1Json/', views.Hp1JsonView, name='hp1json'),
]
