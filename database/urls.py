from django.conf.urls import include, url
from django.contrib import admin
import users.views as UserViews

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^user/', UserViews.Users.as_view()),
    url(r'^school/', include('school.urls')),
]
